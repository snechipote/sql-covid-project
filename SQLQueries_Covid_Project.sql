SELECT *
FROM Coviddeaths
WHERE continent is not null
Order By 3,4


SELECT *
FROM Covidvacinations
WHERE continent is not null
Order By 3,4

--Select the data we are going to be using

SELECT Location,date, total_cases, new_cases, total_deaths, population 
FROM Coviddeaths
WHERE continent is not null
Order By 1,2

--Total Cases vs Total Deaths

ALTER TABLE Coviddeaths
ALTER COLUMN total_deaths float

SELECT Location,date, total_cases, total_deaths, (total_deaths/total_cases)*100 as DeathPercentage
FROM Coviddeaths
WHERE location like '%Zimbabwe%'
Order By 1,2


--Looking at Total Cases vs Population
--Shows what percentage of population got Covid

ALTER TABLE Coviddeaths
ALTER COLUMN total_cases float

SELECT Location,date, population, total_cases,  (total_cases/population)*100 as PercentPopulationInfected
FROM Coviddeaths
WHERE location like '%Zimbabwe%'
Order By 1,2

--Looking at Countries with Highest Infection Rate compared to Population
SELECT Location, population, MAX(total_cases) as HighestInfectionCount,  MAX((total_cases/population))*100 as PercentPopulationInfected
FROM Coviddeaths
--WHERE location like '%Zimbabwe%'
WHERE continent is not null
Group By  Location, population
Order By PercentPopulationInfected desc

--Countries with Highest Death Count Per Population
SELECT Location,  MAX(cast(total_deaths as int)) as TotalDeathCount
FROM Coviddeaths
--WHERE location like '%Zimbabwe%'
WHERE continent is not null
Group By  Location
Order By TotalDeathCount desc


--Breakdown by Continent
SELECT continent,  MAX(cast(total_deaths as int)) as TotalDeathCount
FROM Coviddeaths
--WHERE location like '%Zimbabwe%'
WHERE continent is not null
Group By  continent
Order By TotalDeathCount desc


-- GLOBAL NUMBERS

Select SUM(cast(new_cases as float)) as total_cases, SUM(cast(new_deaths as float)) as total_deaths, SUM(cast(new_deaths as float))/SUM(cast(new_Cases as float))*100 as DeathPercentage
From Coviddeaths
--Where location like '%states%'
where continent is not null 
--Group By date
order by 1,2


-- Total Population vs Vaccinations
-- Shows Percentage of Population that has recieved at least one Covid Vaccine
Select dea.continent, dea.location, dea.date, dea.population, vac.new_vaccinations
, SUM(CONVERT(float,vac.new_vaccinations)) OVER (Partition by dea.Location Order by dea.location, dea.Date) as RollingPeopleVaccinated
--(RollingPeopleVaccinated/population)*100
From Coviddeaths dea
Join Covidvacinations vac
	On dea.location = vac.location
	and dea.date = vac.date
where dea.continent is not null 
order by 2,3



-- Using CTE to perform Calculation on Partition By in previous query

With PopvsVac (Continent, Location, Date, Population, New_Vaccinations, RollingPeopleVaccinated)
as
(
Select dea.continent, dea.location, dea.date, dea.population, vac.new_vaccinations
, SUM(CONVERT(float,vac.new_vaccinations)) OVER (Partition by dea.Location Order by dea.location, dea.Date) as RollingPeopleVaccinated
--, (RollingPeopleVaccinated/population)*100
From Coviddeaths dea
Join Covidvacinations vac
	On dea.location = vac.location
	and dea.date = vac.date
where dea.continent is not null 
--order by 2,3
)
Select *, (RollingPeopleVaccinated/Population)*100
From PopvsVac



-- Using Temp Table to perform Calculation on Partition By in previous query

DROP Table if exists #PercentPopulationVaccinated
Create Table #PercentPopulationVaccinated
(
Continent nvarchar(255),
Location nvarchar(255),
Date datetime,
Population numeric,
New_vaccinations numeric,
RollingPeopleVaccinated numeric
)

Insert into #PercentPopulationVaccinated
Select dea.continent, dea.location, dea.date, dea.population, vac.new_vaccinations
, SUM(CONVERT(float,vac.new_vaccinations)) OVER (Partition by dea.Location Order by dea.location, dea.Date) as RollingPeopleVaccinated
--, (RollingPeopleVaccinated/population)*100
From Coviddeaths dea
Join Covidvacinations vac
	On dea.location = vac.location
	and dea.date = vac.date
--where dea.continent is not null 
--order by 2,3

Select *, (RollingPeopleVaccinated/Population)*100
From #PercentPopulationVaccinated




-- Creating View to store data for later visualizations

Create View PercentPopulationVaccinated as
Select dea.continent, dea.location, dea.date, dea.population, vac.new_vaccinations
, SUM(CONVERT(float,vac.new_vaccinations)) OVER (Partition by dea.Location Order by dea.location, dea.Date) as RollingPeopleVaccinated
--, (RollingPeopleVaccinated/population)*100
From Coviddeaths dea
Join Covidvacinations vac
	On dea.location = vac.location
	and dea.date = vac.date
where dea.continent is not null 

